﻿using System;
using System.Collections.Generic;

namespace LV4_RPPOON2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> bag = new List<IRentable>();
            Video video = new Video("Harry Potter-movie");
            Book book = new Book("Harry Potter-book");
            bag.Add(video);
            bag.Add(book);

            RentingConsolePrinter rent = new RentingConsolePrinter();
            rent.DisplayItems(bag);
            rent.PrintTotalPrice(bag); 


            Console.WriteLine();
            HotItem hitBook = new HotItem(new Book("HitBook"));
            HotItem hitVideo = new HotItem(new Video("HitVideo"));
            bag.Add(hitBook);
            bag.Add(hitVideo);

            rent.DisplayItems(bag);
            rent.PrintTotalPrice(bag); 


            Console.WriteLine();

            double discount = 15;
            List<IRentable> flashsale = new List<IRentable>();
            DiscountedItem discountedBook = new DiscountedItem(book, discount);
            DiscountedItem discountedVideo = new DiscountedItem(video, discount);
            DiscountedItem discountedHitBook = new DiscountedItem(hitBook, discount);
            DiscountedItem discountedHitVideo = new DiscountedItem(hitVideo, discount);

            flashsale.Add(discountedBook);
            flashsale.Add(discountedVideo);
            flashsale.Add(discountedHitBook);
            flashsale.Add(discountedHitVideo);

            rent.DisplayItems(flashsale);
            rent.PrintTotalPrice(flashsale);
        }
    }
}

//main za 3,4,5 zadatak
