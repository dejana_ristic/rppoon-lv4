﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_RPPOON2
{
    class DiscountedItem: RentableDecorator
    {
        private readonly double discount;

        public DiscountedItem(IRentable rentable, double discount) : base(rentable)
        {
            this.discount = discount / 100;
        }

        public override double CalculatePrice()
        {
            double discountPrice;
            discountPrice = base.CalculatePrice() * this.discount;
            return base.CalculatePrice()-discountPrice;
        }

        public override String Description
        {
            get
            {
                return base.Description + " now at " + discount * 100 + "% off!";
            }
        }
    }
}
