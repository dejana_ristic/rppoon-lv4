﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_RPPOON3
{
    interface IPasswordValidatorService
    {
        bool IsValidPassword(String candidate);
    }
}
