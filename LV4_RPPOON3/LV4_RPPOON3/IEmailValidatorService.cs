﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_RPPOON3
{
    interface IEmailValidatorService
    {
        bool IsValidAddress(String candidate);
    }
}
