﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_RPPOON3
{
    class EmailValidator: IEmailValidatorService
    {
        public int MinLength { get; private set; }
        public EmailValidator(int minLength)
        {
            this.MinLength = minLength;
        }
        public bool IsValidAddress(String candidate)
        {
            if (String.IsNullOrEmpty(candidate))
            {
                return false;
            }
           return IsLongEnough(candidate) && ContainsAtCharacter(candidate) && CorrectEnding(candidate);
        }
        private bool IsLongEnough(String candidate)
        {
            return candidate.Length >= this.MinLength;
        }
        private bool ContainsAtCharacter(String candidate)
        {
            return candidate.Contains("@");
        }
        private bool CorrectEnding(String candidate)
        {
            return candidate.EndsWith(".com") || candidate.EndsWith(".hr");
        }

    }
}
