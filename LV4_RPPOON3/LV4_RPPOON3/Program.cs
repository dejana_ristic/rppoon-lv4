﻿using System;
using System.Collections.Generic;

namespace LV4_RPPOON3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> addressList = new List<string>();
            EmailValidator emailValidator = new EmailValidator(10);

            string address1 = "dejanaristic99@gmail.com";
            string address2 = "dejanaristic13@gmail.com";
            string address3 = "d.dejanaristic@gmail.com";
            string address4 = "dristic@etfos.hr";

            addressList.Add(address1);
            addressList.Add(address2);
            addressList.Add(address3);
            addressList.Add(address4);

            foreach (string address in addressList)
            {
                Console.WriteLine(emailValidator.IsValidAddress(address));
            }
            Console.ReadKey();
        }
    }
}
